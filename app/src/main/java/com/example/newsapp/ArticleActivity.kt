package com.example.newsapp

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.example.newsapp.ui.main.ArticleFragment
import com.squareup.picasso.Picasso

class ArticleActivity() : AppCompatActivity() {
    private var articleURL=""

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.article_activity)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setHomeButtonEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ArticleFragment.newInstance())
                .commitNow()
        }
        val tileActivity = findViewById<TextView>(R.id.textView_title)
        val textActivity = findViewById<TextView>(R.id.textView_text)
        val photoView = findViewById<ImageView>(R.id.imageView)
        val intent = getIntent()

        tileActivity.text=intent.getStringExtra("title")
        textActivity.text=intent.getStringExtra("text")
        Picasso.get().load(intent.getStringExtra("photoURL")).error(R.drawable.error_img).into(
            photoView
        )
        this.articleURL=intent.getStringExtra("articleURL") as String

        val btn = findViewById<Button>(R.id.button_toWeb)
        btn.setOnClickListener(View.OnClickListener {
            val address: Uri = Uri.parse(this.articleURL)
            val openLinkIntent = Intent(Intent.ACTION_VIEW, address)
            startActivity(openLinkIntent)
        })
    }
}