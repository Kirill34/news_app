package com.example.newsapp

class NewsAnswer(public val status : String, public val totalResults: Int, public val articles : ArrayList<Article> ) {
}

class Article(public val source : SourceClass, public val author: String, public val title: String, public val description : String, public val url: String, public val urlToImage: String, public val publishedAt : String, public val content : String )
class SourceClass(public val id: String, public val name: String)