package com.example.newsapp

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import com.squareup.picasso.Picasso
import java.net.URL


class NewsAdapter(
    private val context: Context,
    private val dataSource: ArrayList<Article>
) : BaseAdapter() {


    private val inflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater



    override fun getCount(): Int {
        return this.dataSource.size
    }

    override fun getItem(p0: Int): Any {
        return dataSource[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.list_item_news, parent, false)
        val titleView = rowView.findViewById<TextView>(R.id.textView_title)
        val event=getItem(position) as Article
        titleView.text=event.title

        val descriptionView = rowView.findViewById<TextView>(R.id.textView_description)
        descriptionView.text=event.description

        val photoView = rowView.findViewById<ImageView>(R.id.imageView)
        Picasso.get().load(event.urlToImage).error(R.drawable.error_img).into(photoView)

        //val titleView = findViewById<TextView>(R.id.textView_title)

        rowView.setOnClickListener(View.OnClickListener()
        {

            val intent= Intent(this.context,ArticleActivity::class.java)
            intent.putExtra("title",event.title)
            intent.putExtra("text",event.content)
            intent.putExtra("photoURL",event.urlToImage)
            intent.putExtra("articleURL",event.url)
            startActivity(this.context,intent,null)
        })
        return rowView
    }

}