package com.example.newsapp

import android.Manifest
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Website.URL
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import java.io.File
import java.net.URL
import java.util.concurrent.Executors


class MainActivity : AppCompatActivity() {

    private val API_KEY="5071b047646c45a582f647202cbe4a62"
    private val newsapi_URL="http://newsapi.org/v2/everything?q=bitcoin&from=2020-09-21&sortBy=publishedAt&apiKey="+API_KEY
    var listItems=ArrayList<Article>(0)
    var ready=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var gson = Gson()
        Executors.newSingleThreadExecutor().execute({
            val url = URL(newsapi_URL)
            val news_json=url.readText()
            val news=gson.fromJson<NewsAnswer>(news_json,NewsAnswer::class.java)
            this.listItems = news.articles
            this.ready=true
            //val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)

        })
        while(!ready)
        {

        }
        var news_list = findViewById<ListView>(R.id.newslist)
        val adapter = NewsAdapter(this, listItems)
        news_list.adapter = adapter







    }
}